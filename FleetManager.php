<?php namespace PortalSynchronization;

use Exception;
use Webservice\Services\Client as WebServiceClient;

use Respect\Validation\Validator as v;

/**
 * Class FleetManager
 */
class FleetManager
{
    const SOURCE_STAGING = 'staging';
    const SOURCE_PRODUCTION = 'production';

    const PATH_SETTINGS = 'sync/_car.json';
    const PATH_CARS = 'sync/_cars.json';
    const PATH_LOG = 'other/logs/sync.log';
    const PATH_CARLOG = 'other/logs/carstatus.%s.log';
    const PATH_VERSION = 'version';

    const PATH_TRIGGER = 'sync/_switch.json';

    const RSYNC_SHORE = "%s -azP --timeout=180 --delay-updates --log-file=/var/log/rsync.log --exclude 'cculocalconfig.php' --exclude-from '%sdata/sync/excludes.conf' %s %s";
    const RSYNC_SWITCH = "%s -azP --timeout=180 --delay-updates --log-file=/var/log/rsync.log --exclude 'cculocalconfig.php' --exclude-from '%sdata/sync/excludes.conf' %s/ %s";

    const STATUS_OK = 2;
    const STATUS_ERROR = 99;

    protected static $cars;
    protected static $valid_rsync_targets = array('production', 'staging');

    protected static $process_id;
    protected static $rsync_path;


    /** ********************** SHORESIDE Management Functions ****************************** */


    /**
     * list configured cars, reload from directory on force_reload
     * @param boolean $force_reload
     * @return mixed
     * @throws Exception
     * @throws FileNotFoundException
     */
    public static function listCars($force_reload = false)
    {
        if (self::$cars && !$force_reload) {
            return self::$cars;
        }

        $source_file = self::getCarsSource();

        if (!self::isSourceReadable()) {
            throw new \FileNotFoundException('(FleetManager) cars-configuration not found');
            return null;
        }

        $cars = json_decode(file_get_contents($source_file), true);

        // format error
        if (is_null($cars)) {
            throw new \Exception('(FleetManager) cars-configuration invalid: json format has errors');
        }

        // result
        $result = array();
        foreach ($cars as $car) {
            $car['last_log_entry'] = self::getCarLog($car['id'], true);
            $json_car_log = json_decode(self::getCarLog($car['id'], true),true);
            if (!empty($json_car_log['content_version']))
                $car['content_version'] = $json_car_log['content_version'];

            $result[$car['id']] = $car;
        }

        // set for later usage
        self::$cars = $result;

        return $result;
    }


    /**
     * get the id from a given ip address
     * @param $ip_address
     * @return int|null|string
     * @throws Exception
     * @throws \FileNotFoundException
     */
    public static function getIdFromIp($ip_address)
    {
        $cars = self::listCars(true);
        $ip_address = trim($ip_address);



        foreach ($cars as $id => $data) {
            if ($data['ip_address'] == $ip_address)
                return $id;
        }

        return null;
    }


    /**
     * set the portal status for the given train
     *
     * @param string $car_id
     * @param array $log_data
     * @return bool
     */
    public static function logStatusFromCar($car_id, array $log_data)
    {
        // always fetch a fresh copy => maybe some parallel processes did change something
        // => minimise overriding risks
        $cars = self::listCars(true);

        // no changes on unknown cars status'
        if (empty($cars[$car_id])) {
            return false;
        }

        // do the magic
        unset($cars[$car_id]['trigger_update'], $cars[$car_id]['error']);
        $cars[$car_id]['last_update'] = self::getDate();
        $cars[$car_id]['source'] = $log_data['source'];

        // persist the "update status" to cars
        self::persistCarConfig($cars);

        // new, create a log entry for the current status
        $car_logfile = GSDATAPATH . sprintf(self::PATH_CARLOG, \String::slug($car_id));

        if (!file_put_contents($car_logfile, self::formatCarLog($log_data), FILE_APPEND))
            self::log('error on logging car logs');

        return true;
    }


    /**
     * format the data into a loggable string
     * @param array $log_data
     * @return string
     */
    public static function formatCarLog(array $log_data)
    {
        unset($log_data['rq']);

        return json_encode($log_data) . "\n";
    }


    /**
     * remove a car from list
     * @param $car_id
     * @return bool
     * @throws Exception
     * @throws \FileNotFoundException
     */
    public static function removeCar($car_id)
    {
        $cars = self::listCars(true);
        if (!isset($cars[$car_id]))
            return false;

        // remove it
        unset($cars[$car_id]);

        // persist new list
        return self::persistCarConfig($cars);
    }


    /**
     * create a new car and persist, then return the new random car settings
     * @return array|bool
     * @throws Exception
     * @throws \FileNotFoundException
     */
    public static function createNewCar()
    {
        $cars = self::listCars(true);
        $new = array(
            'id' => substr(uniqid(), 0, 10),
            'name' => 'NEW CAR',
            'ip' => '',
            'source' => 'production',
            'secret' => uniqid(),
            'last_update' => '',
            'trigger_update' => date('Y-m-d H:i:s'),
        );

        $cars[$new['id']] = $new;

        if (self::persistCarConfig($cars))
            return $new;

        return false;
    }


    /**
     * update car data to list
     * @param $car_id
     * @param array $data
     * @return bool
     * @throws Exception
     * @throws \FileNotFoundException
     */
    public static function updateCar($car_id, array $data)
    {
        $cars = self::listCars(true);
        if (!isset($cars[$car_id]))
            return false;

        // merge data
        $cars[$car_id] = array_merge($cars[$car_id], $data);

        // if car_id was changed, move it in list
        if ($car_id != $data['id']) {
            $cars[$data['id']] = $cars[$car_id];
            unset($cars[$car_id]);
        }

        // persist and return status
        return self::persistCarConfig($cars);
    }


    /**
     * check for required values
     * @param array $data
     * @return bool
     */
    public static function validateCarData(array $data)
    {
        $ip_valid = v::string()->notEmpty()->string()->validate($data['ip_address']);
        $id_valid = v::string()->notEmpty()->noWhitespace()->length(1,20)->validate($data['id']);
        $name_valid = v::string()->notEmpty()->alnum()->length(1,50)->validate($data['name']);

        return ($ip_valid && $id_valid && $name_valid);
    }

    /**
     * persist the configuration to current system
     * @param array $cars
     * @return bool
     * @throws Exception
     */
    public static function persistCarConfig(array $cars)
    {
        if (!self::isSourceWriteable()) {
            throw new \Exception('(FleetManager) persisting configuration failed: source is not writeable');
        }

        return file_put_contents(
            self::getCarsSource(),
            json_encode(
                $cars,
                JSON_PRETTY_PRINT
            )
        ) ? true : false;
    }


    /**
     * get a valid rsync source target
     * @param string $mode
     * @return string
     */
    public static function getValidSourceTarget($mode = 'production')
    {
        return (strtolower(trim($mode)) === 'staging') ? self::SOURCE_STAGING : self::SOURCE_PRODUCTION;
    }


    /**
     * set all required parameters for update
     * @param array $car
     * @param string $source
     * @return array
     */
    public function markForUpdate(array $car, $source)
    {
        $car['source'] = $source;
        $car['trigger_update'] = self::getDate();

        return $car;
    }


    /**
     * check if car has an update flag
     * @param array $car
     * @return bool
     */
    public function hasUpdateFlag(array $car)
    {
        return empty($car['trigger_update']) ? false : true;
    }


    /**
     * get the source
     * @return string
     */
    public static function getCarsSource()
    {
        return GSDATAPATH . self::PATH_CARS;
    }


    /**
     * check if source file is readable
     * @return bool
     */
    public static function isSourceReadable()
    {
        return is_readable(self::getCarsSource());
    }


    /**
     * check if source file is writeable
     * @return bool
     */
    public static function isSourceWriteable()
    {
        return is_writable(self::getCarsSource());
    }


    /**
     * check if source file is accessible for updates
     * @return bool
     */
    public static function hasSourceAccess()
    {
        return self::isSourceReadable() && self::isSourceWriteable();
    }


    /**
     * get the current date in a defined format
     * @param string $format
     * @return bool|string
     */
    protected static function getDate($format = 'Y-m-d H:i:s')
    {
        return date($format);
    }

    /**
     * set an error message for the car
     * @param array $car
     * @param $error_message
     * @return array
     */
    public static function setError(array $car, $error_message)
    {
        $car['error'] = $error_message;
        $car['last_update'] = self::getDate();

        return $car;
    }

    /**
     * get an error message for the car
     * @param array $car
     * @return string
     */
    public static function getError(array $car)
    {
        return empty($car['error']) ? null : $car['last_error'];
    }

    /**
     * check for errors
     * @param array $car
     * @return bool
     */
    public static function hasError(array $car)
    {
        return empty($car['error']) ? false : true;
    }

    /**
     * get the logfile entries for the given car
     *
     * @param string $car_id
     * @param bool $last_entry_only
     * @return mixed|string
     */
    public static function getCarLog($car_id, $last_entry_only=true)
    {
        $car_logfile = GSDATAPATH . sprintf(self::PATH_CARLOG, $car_id);

        if (!is_readable($car_logfile))
            return false;

        $logs = explode("\n", trim(file_get_contents($car_logfile)));

        return $last_entry_only && is_array($logs) ? array_pop($logs) : $logs;
    }


    /** *************************** SERVICES - Update Car / portal status **************************** */

    /**
     * @return array
     * @throws Exception
     */
    public static function execSyncWithShoreside()
    {
        // prevent any sync with non-car systems => this might lead to overriding dev/backend environments
        if (false === (defined('IS_CAR') && IS_CAR === true)) {
            self::log('### terminated sync process - please set IS_CAR to true if this system is a car ####');
            throw new \Exception("### terminated sync process - please set IS_CAR to true if this system is a car ####\n");
        }

        // prevent any sync with unset sync folder targets
        if (false == (defined('BUFFER_FOLDER') && defined('SHADOW_FOLDER'))) {
            self::log('### terminated sync process - you MUST provide valid settings for shadow/bufer folder ####');
            throw new \Exception("### terminated sync process - you MUST provide valid settings for shadow/bufer folder ####\n");
        }

        if (false == (is_writeable(BUFFER_FOLDER) && is_writable(SHADOW_FOLDER))) {
            self::log('### terminated sync process - shadow/buffer folder must be writeable ####');
            throw new \Exception("### terminated sync process - shadow/buffer folder must be writeable  ####\n");
        }

        // create a process id for every "call" of this method as one process which will order the log entries
        self::setProcessId(date('Y-m-d H:i:s'));

        $settings = array_merge(self::getLocalSettings(), self::getConfig());
        $results = array();

        // log the configuration
        self::log('### new synchronisation process ####');

        /**
         *
         */
        $statfile = GSDATAPATH . 'sync/logs/statfile';

        if (file_exists($statfile)) {
            sleep(15);

            for($i = 0; $i < 60; $i++) {
                clearstatcache();

                if (file_exists($statfile)) {
                    sleep(15);
                }
            }
        }

        file_put_contents($statfile, 1);

        /**
         *
         */
        if (!self::validateConfig($settings, $results)) {
            unlink($statfile);
            return $results;
        }

        // a new target was set which is not the current target+
        // We don't wanna start rsync stuff unless the _switch.json file flag is there.
        $new_target = self::getNewTarget();

        if ($new_target) {
            $settings['target'] = $new_target;

            self::log('new target: '.$new_target);

            // save that new setting first
            $content = json_encode(['target' => $new_target]);

            if (!file_put_contents(GSDATAPATH . self::PATH_SETTINGS, $content)) {
                unlink($statfile);
                self::log('unable to save new target, exiting with error');
                throw new \Exception('unable to save new target, exiting with error');
            }

            // remove NewTargetTrigger file
            self::resetNewTargetTrigger();
        }

        // buffer synchronisation failed => just return without further processing
        if (!self::syncBufferWithShoreside($settings, $results)) {
            unlink($statfile);
            return $results;
        }

        // everything seems to be alright, so do the final magic and pray it works out
        self::syncBufferToHtdocs($settings, $results);

        unlink($statfile);

        return $results;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public static function execResetFromShadow()
    {
        // prevent any sync with non-car systems => this might lead to overriding dev/backend environments
        if (false === (defined('IS_CAR') && IS_CAR === true)) {
            self::log('### terminated sync process - please set IS_CAR to true if this system is a car ####');
            throw new \Exception("### terminated sync process - please set IS_CAR to true if this system is a car ####\n");
        }

        // create a process id for every "call" of this method as one process which will order the log entries
        self::setProcessId(date('Y-m-d H:i:s'));

        $settings = array_merge(self::getLocalSettings(), self::getConfig());
        $results = array();

        // log the configuration
        self::log('### new backup process started ####');

        // everything seems to be alright, so do the final magic and pray it works out
        return self::syncShadowToHtdocs($settings, $results);
    }


    /**
     * sync the buffer with shoreside rsync
     *
     * @param array $settings
     * @param array $results
     * @return bool
     */
    public static function syncBufferWithShoreside(array $settings, array &$results)
    {
        $rsync = self::getRsyncPath();

        // command requires rsync, path to root, rsync-server-source and target
        $command = sprintf(self::RSYNC_SHORE,
            $rsync,
            GSROOTPATH,
            $settings['shore_rsync'][$settings['target']],
            $settings['folders']['buffer']
        );
        self::log('command: '.$command);
        
        if (!self::execCommand($command, $results))
            return false;

        $results['status'] = self::STATUS_OK;

        // log successfull buffer sync
        self::log('syncing shorside-to-buffer successfull');

        return true;
    }


    /**
     * sync htdocs to a shadow / backup folder
     *
     * @param array $settings
     * @param array $results
     * @return bool
     */
    public static function syncShadowFromHtdocs(array $settings, array &$results)
    {
        $rsync = self::getRsyncPath();

        // command requires rsync, path to root, source folder and target folder
        $command = sprintf(self::RSYNC_SWITCH,
            $rsync,
            GSROOTPATH,
            GSROOTPATH,
            $settings['folders']['shadow']
        );

        if (!self::execCommand($command, $results))
            return false;

        $results['status'] = self::STATUS_OK;

        // log successfull buffer sync
        self::log('syncing shadow from htdocs successfull');

        return true;
    }


    /**
     * sync the buffer to htdocs
     *
     * @param array $settings
     * @param array $results
     * @return bool
     */
    public static function syncBufferToHtdocs(array $settings, array &$results)
    {
        // command requires rsync, path to root, source folder and target folder
        $command = sprintf(self::RSYNC_SWITCH,
            self::getRsyncPath(),
            GSROOTPATH,
            $settings['folders']['buffer'],
            GSROOTPATH
        );

        if (!self::execCommand($command, $results))
            return false;

        $results['status'] = self::STATUS_OK;

        // log successfull buffer sync
        self::log('syncing htdocs from buffer successfull');

        return true;
    }


    /**
     * sync the shadow back to htdocs
     *
     * @param array $settings
     * @param array $results
     * @return bool
     */
    public static function syncShadowToHtdocs(array $settings, array &$results)
    {
        // command requires rsync, path to root, source folder and target folder
        $command = sprintf(self::RSYNC_SWITCH,
            self::getRsyncPath(),
            GSROOTPATH,
            $settings['folders']['shadow'],
            GSROOTPATH
        );

        if (!self::execCommand($command, $results))
            return false;

        $results['status'] = self::STATUS_OK;

        // log successfull buffer sync
        self::log('resetting htdocs from shadow successfull');

        return true;
    }


    /**
     * @param string $command
     * @return bool
     */
    protected static function execCommand($command, &$results)
    {
        $output = $exec_status = null;

        $command .= ' 2>&1';

        // log the command for debugging
        self::log(sprintf('execute command: %s', $command));

        @exec($command, $output, $exec_status);

        // zero means command executed successfully
        if ($exec_status === 0)
            return true;

        // set error message
        $results['error'] = implode(' ', $output);
        $results['status'] = self::STATUS_ERROR;

        // log the command for debugging
        self::log(sprintf('command execution failed with error: %s', implode('', $output)));

        return false;
    }


    /**
     * validate the configuration settings and return error messsage
     *
     * @param array $config
     * @param array $results
     * @return bool
     */
    protected function validateConfig(array $config, array &$results)
    {
        // rsync targets
        if (empty($config['shore_rsync']['production']) || empty($config['shore_rsync']['staging'])) {
            $results['error'] = '(Fleet Manager) - invalid configuration, missing shore_rsync settings';
            self::log(sprintf('configuration error: %s', json_encode($config)));
            return false;
        }

        // local rsync target setting
        if (empty($config['target']) || !in_array($config['target'], self::$valid_rsync_targets)) {
            $results['error'] = '(Fleet Manager) - invalid or missing rsync target';
            self::log(sprintf('configuration error: %s', json_encode($config)));
            return false;
        }

        // shadow folder
        /*
        if (!is_writeable($config['folders']['shadow'])) {
            $results['error'] = '(Fleet Manager) - shadow folder not writeable';
            self::log(sprintf('shadow folder not writeable for webservice: %s', json_encode($config)));
            return false;
        }

        // buffer folder
        if (!is_writeable($config['folders']['buffer'])) {
            $results['error'] = '(Fleet Manager) - buffer folder not writeable';
            self::log(sprintf('buffer folder not writeable for webservice: %s', json_encode($config)));
            return false;
        }
        */

        // log status
        self::log(sprintf('configuration and system ready: %s', json_encode($config)));
        return true;
    }


    /**
     * get an array of local vehicle-settings
     * @return array
     */
    protected static function getLocalSettings()
    {
        // load local settings or create the settings file first
        if (file_exists(GSDATAPATH . self::PATH_SETTINGS)) {
            $settings = json_decode(file_get_contents(GSDATAPATH . self::PATH_SETTINGS), true);
        } else {
            $settings = array('target' => 'production');
        }

        // oh oh .... file exists, but is empty :-(
        if (!$settings || !is_array($settings)) {
            throw new \ErrorException('(FleetManager) Failed to read / create local train sync configuration');
        }

        return $settings;
    }


    /**
     * get the sync configuration
     * @return array
     */
    protected static function getConfig()
    {
        // load configuration
        $config = new \Configuration('portalsynchronizationplugin');

        if (!$config) {
            throw new \InvalidArgumentException('(FleetManager) failed to load configuration');
        }

        // set rsync services
        return array(
            'shore_rsync' => array(
                'staging' => trim((string)$config->shoresync_staging),
                'production' => trim((string)$config->shoresync_production),
            ),
            'folders' => array(
                'buffer' => defined('BUFFER_FOLDER') ? BUFFER_FOLDER : null,
                'shadow' => defined('SHADOW_FOLDER') ? SHADOW_FOLDER : null,
            ),
            'webservice' => trim((string)$config->shoresync_webservice)
        );
    }


    /**
     * create a log entry in the local logfile
     * @param $message
     * @return int
     */
    public static function log($message)
    {
        return file_put_contents(GSDATAPATH . self::PATH_LOG, sprintf("%s# %s\n", self::$process_id, $message), FILE_APPEND);
    }


    /**
     * set the process id
     *
     * @param $id
     */
    protected static function setProcessId($id)
    {
        self::$process_id = $id;
    }


    /**
     * get the current content version from GSROOTPATH/version
     *
     * @return string
     */
    public static function getContentVersion()
    {
        $version = trim(file_get_contents(GSROOTPATH . self::PATH_VERSION));
        return  $version ?: 'no-version-set';
    }


    /**
     * get the path to rsync on system
     *
     * @return string
     */
    public static function getRsyncPath()
    {
        if (self::$rsync_path)
            return self::$rsync_path;

        $server_path = explode(":", $_SERVER["PATH"]);
        foreach ($server_path as $path) {
            if (is_file($path . "/rsync")) {
                self::$rsync_path = $path . "/rsync";
                return self::$rsync_path;
            }
        }

        throw new \ErrorException('(FleetManager) finding path to rsync failed - maybe not installed?');
    }


    /**
     * check / get a new target from the target-switch trigger
     *
     * @return string | false
     */
    public static function getNewTarget()
    {
        if (!file_exists(GSDATAPATH . self::PATH_TRIGGER))
            return false;

        $rs = json_decode(file_get_contents(GSDATAPATH . self::PATH_TRIGGER), true);

        return !empty($rs['newtarget']) ? $rs['newtarget'] : false ;
    }


    /**
     * remove the NewTargetTrigger if any exists
     */
    public static function resetNewTargetTrigger()
    {
        if (file_exists(GSDATAPATH . self::PATH_TRIGGER))
            unlink(GSDATAPATH . self::PATH_TRIGGER);
    }


    /**
     * create a new SwitchTargetTrigger file
     *
     * @param string $target
     * @return bool
     */
    public static function createNewTargetTrigger($target)
    {
        // prevent setting a trigger from the webservice on the shoreside machine...
        if (defined('IS_CAR') && IS_CAR !== true) {
            self::log('### terminated sync process - please set IS_CAR to true if this system is a car ####');
            throw new \Exception("### terminated sync process - please set IS_CAR to true if this system is a car ####\n");
        }

        $target = strtolower(trim($target));

        if (!in_array($target, self::$valid_rsync_targets))
            throw new \InvalidArgumentException('tried to set an invalid rsync resource target');

        // ok...so now when we ensured the validity of the value, get the target and compare
        $settings = self::getLocalSettings();

        if ($target === $settings['target'])
            return null;

        return file_put_contents(GSDATAPATH . self::PATH_TRIGGER, json_encode(array('newtarget' => $target))) ? true : false;
    }


    /* ************************* WEBSERVICE COMMUNICATION HANDLER ************************************/

    /**
     * this method connects to the webservice in the vehicles, triggering a target update on the vehicle which
     * will lead to a new synchronization target and maybe update / reset process
     *
     * @param array $car
     * @return array
     */
    public static function triggerTargetSwitchOnCar(array $settings)
    {
        if (empty($settings['ip_address']))
            throw new \Exception('Missing target ip address on webservice call');

        // call webservice on car to create the target trigger
        $webservice_uri = sprintf('http://%s/webservice/index.php', $settings['ip_address']);

        $client = new WebServiceClient($webservice_uri);
        try {
            $client->call('chngtrgt', array('target' => $settings['source']));

        } catch (\Exception $e) {

            // save the error to the car
            $settings['error'] = 'Remote update failed - please check the logfiles for reasons';
            FleetManager::updateCar($settings['id'], $settings);

            throw new \Exception($e->getMessage());
        }
    }


    /**
     * this method will connecto to shoreside and trigger the update process so the car status is updated
     * to the current sync status. This is called from the Oberserver after the update was successfull
     *
     */
    public static function udpateStatusOnShoreside(array $results)
    {
        $settings = self::getLocalSettings();
        $config = self::getConfig();

        if (empty($config['webservice']))
            throw new \Exception('Missing shoreside webservice configuration');

        $log_data = array(
            'ip_address' => CAR_IP,
            'source' => $settings['target'],
            'update_time' => date('Y-m-d H:i:s'),
            'update_status' => empty($results['status']) ? self::STATUS_OK : $results['status'],
            'content_version' => FleetManager::getContentVersion(),
        );

        // has errors
        if (!empty($results['error'])) {
            $log_data['error_message'] = $results['error'];
        }

        // connect to webservice and call method
        $client = new WebServiceClient($config['webservice']);
        $client->call('logstt', $log_data);
    }
}


