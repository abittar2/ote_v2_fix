#!/bin/bash

CARS=("via.3464" "via.3468" "via.3471" "via.3472" "via.3476" "via.7301")
BENCHES=("via.4015" "via.4016" "via.4018" "via.4019" "via.4020")
OFFLINE=("via.4015" "via.4016" "via.4020")
ALL=(${CARS[*]} ${BENCHES[*]})

FILES_TO_UPLOAD=("/conf/extra/etc/fcrontab" "/conf/extra/etc/cron.hourly/v2_video_updates" "/conf/extra/etc/cron.hourly/wowza_log_sync.sh" "/mnt/md0/nd_portal/plugins/PortalSynchronizationPlugin/libraries/FleetManager.php" "/mnt/md0/nd_portal/data/configs/portalsynchronizationplugin.xml")

mkdir -p project.confs

for CAR in "via.4019" ; do
	echo ${CAR}
	#scp -P 8022 -o ConnectTimeout=3 ${CAR}:/conf/PROJECT.conf project.confs/PROJECT.conf.8022.${CAR}
	#if [ "$?" != "0" ]; then 
	#	echo "Unable to scp from $CAR !!!!!!!!!!!!!!!!!!!!!!!!!"
	#fi
	for FILE in ${FILES_TO_UPLOAD[*]} ; do
		#create a backup of all the files
		CMD="cp ${FILE} ${FILE}.bak"
		SSH_CMD="ssh -o ConnectTimeout=3 -p 8022 ${CAR} ${CMD}"

		#echo $SSH_CMD

		$SSH_CMD
		if [ "$?" != "0" ]; then
			echo "${CAR} : Unable to create backup for ${FILE}!!!!"
			continue
		fi

		# copy the new files in their place
		CMD="scp -o ConnectTimeout=3 -P 8022 $(basename ${FILE}) ${CAR}:${FILE}"
		#echo "$CMD"
		$CMD
		if [ "$?" != "0" ]; then
			echo "${CAR} : Unable to copy ${FILE}!!!!"
			continue
		fi

		CMD="ls -la ${FILE}"
		SSH_CMD="ssh -o ConnectTimeout=3 -p 8022 ${CAR} ${CMD}"

		#echo $SSH_CMD

		$SSH_CMD
	done

	# create project.conf backup
	CMD="cp /conf/PROJECT.conf /conf/PROJECT.conf.bak"
	SSH_CMD="ssh -o ConnectTimeout=3 -p 8022 ${CAR} ${CMD}"
	echo $SSH_CMD
	$SSH_CMD
	if [ "$?" != "0" ]; then
		echo "${CAR} : Unable to backup Project.conf!!!!"
		continue
	fi

	# modify PROJECT.conf to contain the new lines for WOWZA and CMS IP addresses
	CMD="sed '/usermod -d \/var\/jail\/lighttpd -s \/bin\/bash www/ a\ \n# Assign a central place to change CMS_SERVER_IP and WOWZA_SERVER_IP\nmkdir -p /etc/portal\necho \"172.16.215.210\" > /etc/portal/cms_ip\necho \"172.16.216.200\" > /etc/portal/wowza_ip\n' -i /conf/PROJECT.conf"
	SSH_CMD="ssh -o ConnectTimeout=3 -p 8022 ${CAR} ${CMD}"

	echo $SSH_CMD

	$SSH_CMD
	if [ "$?" != "0" ]; then
		echo "${CAR} : Unable to create backup for ${FILE}!!!!"
		continue
	fi
done
