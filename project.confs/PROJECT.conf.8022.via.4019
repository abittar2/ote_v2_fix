#$Revision: 4881 $
#$Date: 2009-10-16 11:41:59 +0100 (Fri, 16 Oct 2009) $

# This is not the canonical example - that may be found in /conf of any
# recent CCU build; this is from the portal.tar.gz in /conf/boot

# Changelog
# 150114 - wrf/Hi - Changed [webserver] webroot, Added [lighttpd] section, cleared [files] section

[ccu]
ccu_address = 172.19.119.1

[httpd]
ServerName = train.example.com


[uigateway]
remote_aaa = $ccu_address
remote_aaa_port = 8000
timeout = 50000


# Do not edit anything below this line
# 
# -------------------------------------
#
# NO USER SERVICABLE PARTS INSIDE

# stop unified_switcher [FAIL]ing on startup
[loadbalance]
# this clause intentionally left empty

[internal_network]
# Number picked pseudo-randomly to avoid potential collisions. It's intended to 
# be a /24
internal_network_24 = 172.19.119.0
# Again - pseudo-random number - note that this is specified literally in the 
# [networks] section.
internal_network_vlan = 3854

# Convenience
internal_network_ip = ($internal_network_24 + 2)
internal_network_gateway = ($internal_network_24 + 1)

[ccu]
# This will get overridden in MACHINE.conf. Hopefully
#designation = designation

[cron.reboot]
# 0100 UTC
hour = 1

[site]
nameserver = $internal_network_gateway

[ntp]
# come up as client only
ntpserver = no

[networks]
# eth2 will not exist on F15's - this should not be a  problem beyond two 
# [FAIL] appearing on startup. IP address picked from link-local subnet
eth0 = 172.19.119.2 netmask 255.255.255.0
# wrf 150113 - Delete next line in future
#eth0:100 = 192.168.100.2 netmask 255.255.255.0
br0 = eth1 eth2 169.254.176.223 netmask 255.255.255.0
# VLAN number should equal $internal_network_vlan
#eth0.3854 = ($internal_network_ip) netmask 255.255.255.0
#eth1 = 192.168.76.15 netmask 255.255.255.0

[webserver]
webserver = lighttpd
#webserver = httpd
webroot = /mnt/md0/nd_portal

[webcache]
# While this is enabled, current thinking is that it's a too roundabout
# way to use squid on here, plus it can't be done transparently
webcache = on
include = /var/ND/config/squid.conf

[httpd]
# Remove global listen
-Listen = 80
block = <<
LogLevel error
Listen ($internal_network_ip):80
Listen ($internal_network_ip):443

#Include /var/ND/config/nonssl_portal.conf
#Include /var/ND/config/ssl_portal.conf

#Include /etc/sanity.httpd.conf

<Directory /var/ND/sanity>
	Order deny,allow
	Deny from all
	Allow from ($internal_network_24)/24
</Directory>
# End of Sanity checking

# PHP
ScriptAlias /usr-bin /usr/bin
AddHandler application/x-httpd-php5 php
Action application/x-httpd-php5 /usr-bin/php-cgi
<Directory "/usr/bin">
        DirectoryIndex index.php
        Order allow,deny
        Allow from all
</Directory>
>>

[lighttpd]
##MM - The lighttpd section below is specified to enable lighttpd to start automatically, do not delete


server.modules = << \(
    "mod_access",
    "mod_alias",
    "mod_accesslog",
#   "mod_compress",
    "mod_fastcgi",
    "mod_rewrite",
    "mod_redirect",
    "mod_setenv"
\)


>>

# Move section above >> to be used
# \$SERVER["socket"] == "0.0.0.0:443"\{
#	server.document-root = "/mnt/md0/nd_portal"
#	ssl.engine = "enable"
#	ssl.pemfile = "/etc/lighttpd/ssl/portal/server.pem"
# \}

mimetype.assign = << \(
    ".css" => "text/css",
    ".png" => "image/png",
    ".html" => "text/html", 
	".webm" => "video/webm",
	".weba" => "audio/webm",
	".ogv" => "video/ogg",
	".oga" => "audio/ogg",
	".mp4" => "video/mp4",
	".svg" => "image/svg+xml",
	".ttf" => "application/x-font-ttf",
	".otf" => "application/x-font-opentype",
	".woff" => "application/x-font-woff",
	".eotlite" => "application/vnd.ms-fontobject",
	".eot" => "application/vnd.ms-fontobject"
\)

>>
# url-access-deny = \( "~", ".inc", ".local", ".conf",\)
static-file.exclude-extensions = \( ".php", ".pl", ".fcgi", \)
server.pid-file = "/var/run/lighttpd.pid",
server.dir-listing = "disable"

fastcgi.server = << \( 
	".php" => \(\(
		"bin-path" => "/usr/bin/php-cgi",
		"socket" => "/tmp/php.socket",
		"max-procs" => 2,
		"idle-timeout" => 10,
		"bin-environment" => \(
			"PHP_FCGI_CHILDREN" => "4",
			"PHP_FCGI_MAX_REQUESTS" => "10000"
		\),
		"bin-copy-environment" => \(
			"PATH", "SHELL", "USER"
		\),
		"broken-scriptfilename" => "enable"
	\)\)
\)
>>


[files]
/etc/rc.d/rc.local,0700,root:root = <<
#!/bin/sh
route add default gw 172.19.119.1

# wrf 150415 - Copy chilli_response to jail 
#/usr/local/bin/l2chroot /sbin/chilli_response lighttpd	
# touch /var/jail/lighttpd/var/run/chilli.eth2.sock
# chmod 777 /var/run/chilli.eth2.sock
# mount --bind /var/run/chilli.eth2.sock /var/jail/lighttpd/var/run/chilli.eth2.sock



#copy existing ssh keys on boot to allow remote ssh
cp -pv /conf/authorized_keys /root/.ssh &

# wrf 150903 - Fix to use rsync in jail and have a missing php lib in jail
#l2chroot /usr/bin/rsync httpd/
#cp /usr/lib/libltdl.so.3 /var/jail/httpd/usr/lib

# Fix www user to make it work with cron. Used for portal task to sync the portal.
usermod -d /var/jail/lighttpd -s /bin/bash www

>>
