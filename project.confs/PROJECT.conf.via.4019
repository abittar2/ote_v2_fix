#Customer: VIA Rail (Canada)
#Project Code:
#CCU Firmware Version: 4.19.3-1 (Release)
#Patches Applied:
#Configuration Version: v7.5-OTEv2
#AAA System: AccessController
#Date: 16 March 2017
#Author: Clay Michaels

################################################################
#Complete all Change Control details here
################################################################

#Configuration Version: v7.5-OTEv2
#Date: 16 March 2017
#Author: Clay Michaels
#CR Number:xxxx (in Remedy Force)
#  Via tracking under CR 1492
#1. Added Geozones back in. Inadvertently removed in 7.3.
#2. For OTEv2 - added www.apple.com to [walledgarden] whitelist
#	Required for IOS pop-ups. Not in v1, though. Need to investigate.
#3. For OTEv2 - changed portal port forward from 22 to 2510.

#Configuration Version: v7.4
#Date: 8 March 2017
#Author: Clay Michaels
#CR Number:1535 (in Remedy Force)
#  Via tracking under CR 1492
#1. Delete [ospf]coalesce=1
#   It breaks routing and causes AP02s to be unreachable.

#Configuration Version: v7.3
#Date: 8th February 2017
#Author: Tony Santos
#CR Number:1492 (in Remedy Force)
#1. Migrate old configuration to new standard
#2. Correct DNS configuration for DNSMASQ
#3. Add UDP 9090 to IPTable rule to mitigate exploit
# Clay's edits
# Commented out Proxim VLAN and two iptables rules that reference it.
#	ip_proxim = (10.104.0.1 + ($trainnum * 256))
#	iptables -I INPUT -s $ip_apps2/25 -p tcp -m multiport --dport 20,21,22,23,1053,111,26,49153,8005 -d $ip_proxim -j DROP
#	iptables -I INPUT -s $ip_apps2/25 -p udp -m multiport --dport 161,162 -d $ip_proxim -j DROP
#	It is necessary to remove the iptables rules as well, because removing 
#		only the IP address under [ccu] results in a broken reference on boot. 
#	Fascinatingly, commenting out the iptables rules does NOT work. They must be removed.
# Commented out duplicate [webredirect]log_trapped_url = yes

#Configuration Version: v7.2.5 Metering and Throttling change
#Date: 7th Dec 2017
#Author: Clay Michaels
# Changed bwlimit from 1mbps default to 2mbps for 1st class. 
# Removing LI791 (bwlimit) patch because it only works on one VLAN.

#*************************************************
# START OF MANDATORY SETTINGS FOR CCU TO HA COMMS
#*************************************************


################################################################
#Complete all NOC configuration variables here
################################################################

[noc]
ip_fleetmanager = 172.16.216.42
pingroot     = 75.98.19.189
zabbix_server     = 172.16.216.201

################################################################
#Complete all CCU Tunnel details here
################################################################

[nagtun.default]
server_ip           = 75.98.19.156,75.98.19.172,75.98.19.188
encryption           = aes128
key               = 0123456789abcdef0123456789abcdef0123456789ABCDEF0123456789ABCDEF
response_retries      = 6
keep_alive           = 10
auth_timeout          = 15
rekey_period         = 60

[loadbalance]
rtt_ok              = 3000
rtt_max          = 5000

################################################################
#Complete all Geozones details here
################################################################

[zones]
zones = on

# DEPOT ZONES
geozone = Montreal_Depot -73.540761 45.486114 -73.539893 45.477716 -73.557467 45.470796 -73.560642 45.477323
geozone = Toronto_Depot -79.518738 43.604924 -79.500235 43.613947 -79.507262 43.607709
geozone = Quebec_Depot -71.195733 46.838902 -71.191038 46.832590 -71.206242 46.825295 -71.214352 46.831925

orzone = depot_zones Montreal_Depot Toronto_Depot Quebec_Depot
output = depot_zones /conf/scripts/depot_zone.sh 600

################################################################
#Complete all QOS details here
################################################################

[qos]
mode              = lowest_latency
default_service         = low
allow_packet_dropping      = no
high_band          = 2000
medium_band          = 3000
low_band          = 4000
qos_pps_reserve      = 10

################################################################
#Complete all WAN details here
################################################################

##Default device (Ethernet WAN) for benches
#[wan1]
#driver = defaultdevice
#pref = 100000
#ping_host = ($pingroot):7877
#timeout = 1500
#gw = dhclient
#dhcp_timeout = 10
#device_if = eth6
#rssi = 50
#rate = 100000000

[wan1]
driver =        defaultdevice
pref =          100000
ping_host =     ($pingroot):7877
timeout =       1500
gw =            dhclient
dhcp_timeout =  10
device_if =     eth2
rssi =          50
rate =          1000000000


##WAN1 - Bell HSPA+ network
# [wan1]
# driver          = mc8801
# provider     = bell.ca
# apn         = mnet.bell.ca.ioe
# ping_host     = ($pingroot):7877
# slot         = 3
# port         = 0
# hso         = Yes
# pref         = 0
# opsys         = 3,1
# debug         = /dev/tty10
# timeout_2g     = 1000
# timeout_3g     = 1000
# hspa_max     = 5000000
# hspap_max     = 5000000
# sig_lo         = 0
# sig_lolo     = 0
# txqueue         = 100
# reuse_sockets     = yes
# reset_on_startup = yes
# #disable_idle_backoff = yes
# #no_data_disconnect = 90


# ##WAN2 - Rogers LTE network
# [wan2]
# driver         = mc7710
# provider     = rogers.lte
# apn         = m2minternet.apn
# ping_host     = ($pingroot+1):7877
# slot         = 3
# port         = 1
# reset_on_startup = yes

# ##WAN3 - Rogers HSPA+ network
# [wan3]
# driver          = mc8801
# provider     = rogers.ca
# apn         = m2minternet.apn
# ping_host     = ($pingroot+2):7877
# slot         = 4
# port         = 0
# hso         = Yes
# pref         = 0
# opsys         = 3,1
# debug         = /dev/tty10
# timeout_2g     = 1000
# timeout_3g     = 1000
# hspa_max     = 5000000
# hspap_max     = 5000000
# sig_lo         = 0
# sig_lolo     = 0
# txqueue         = 100
# reset_on_startup = yes
# #disable_idle_backoff = yes
# #no_data_disconnect = 90

# ##WAN4 - Rogers LTE network
# [wan4]
# driver         = mc7710
# provider     = rogers.lte
# apn         = m2minternet.apn
# ping_host     = ($pingroot+3):7877
# slot         = 4
# port         = 1
# reset_on_startup = yes

# ##WAN5 - Rogers LTE network
# [wan5]
# driver            = mc7710
# provider     = rogers.lte
# apn         = m2minternet.apn
# ping_host     = ($pingroot+4):7877
# slot         = 5
# port         = 0
# reset_on_startup = yes

# #WAN6 - Rogers LTE network
# [wan6]
# driver         = mc7710
# provider     = rogers.lte
# apn         = m2minternet.apn
# ping_host     = ($pingroot+5):7877
# slot         = 5
# port         = 1
# reset_on_startup = yes

# # Bell LTE - mc770x
# [wan7]
# driver          = mc7710
# provider     = bell.lte
# apn         = mnet.bell.ca.ioe
# ping_host      = ($pingroot+6):7877    
# slot         = 6    
# port         = 0
# reset_on_startup = yes

# # Bell LTE - mc770x
# [wan8]
# driver         = mc7710
# provider     = bell.lte
# apn         = mnet.bell.ca.ioe
# ping_host     = ($pingroot+7):7877    
# slot         = 6    
# port         = 1
# reset_on_startup = yes

##################################################
#Complete all SuperWAN details here
##################################################

##WAN9 - SuperWAN (used when additional capacity required)

[wan9]
driver             = superwan
#Removes additional latency from SuperWAN to force use
rtt_offset         = 0
#Removes ICL link RTT time (SuperWAN RTT) to make WAN's more favourable
use_inter_carriage_rtt     = yes
failover        = auto2
#Buffer period before SuperWAN drops (5000 tested as OK on Motorola AP)
ccu_timeout        = 5000
ccu_request_period    = 1000

################################################################
#Complete all Telemetry / DNA details here
################################################################

[scout]
community            = 7131APr3ad-VIA
telem_port           = 9693
loop_seconds         = 60
interfaces           = br0.100
additional_ip        = $ip_portal
ntp_max_stratum      = 6
number_fast_ping_cycles    = 3
ping_interval_ms           = 25
ping_interval_ms_secondary = 75
ntp_required               = NO

[health]
telem_port            = 9692
check_period          = 15
check_rounds          = 3
ntp_max_stratum       = 6
ntp_required          = NO

[rtlogger]
gather_interval          = 1000
gather_interface         = ndtun1
gather_interface_samples = 5
gps_timeout              = 5000
local_log_multiplier     = 0
remote_log_multiplier    = 10
local_log                = /mnt/hd/rtl%Y%m%d%H00.log
remote_db_port           = 2048
remote_db_delta_compression   = 0
remote_db_maximum_sequence_id = 9999
remote_db_zlib_compression    = 0
wan_query_port           = 2048

[rtclientd]
remote_db_port      = 8051

[rtclientd.aaa.ack]
ack_required        = yes  
ack_timeout         = 10000 

################################################################
#Complete all CCU configuration variables here
################################################################

[ccu]
restart_client_apps     = /conf/scripts/restart_zabbix.sh
disable_all_client_apps = /conf/scripts/disable_zabbix.sh
dmh_retries     = 240
designation     = $serial
trainnum        = $designation[-2]
ip_mgmt         = 10.120.0.1
ip_apps1        = (10.101.0.1 + ($trainnum * 256))
ip_apps2        = (10.(($trainnum)+0).102.1) 
ip_clients      = (10.(($trainnum)+0).0.1)
ip_inter_ccu_comms = (10.103.1.($noc_fleet*100 + ($trainnum+0)))
ip_portal       = (172.19.($noc_fleet*100 + ($trainnum))).2
global_ip       = (10.120.4.($noc_fleet*100 + ($trainnum)))
standby_1       = 0.0.0.0
standby_2       = 0.0.0.0
standby_3       = 0.0.0.0
standby_4       = 0.0.0.0
standby_5       = 0.0.0.0
ip_heartbeat    = $ip_inter_ccu_comms

##################################################
#Complete Multi-CCU and SuperWAN information here
##################################################

heartbeat_timeout    = 60
heartbeat_period    = 10
heartbeat_vlan        = 103
heartbeat_port        = 9703
heartbeat_global_ip    = ($global_ip)

################################################################
#Complete all CCU services configuration here
################################################################

[ospf]
enable             = 1
period             = 30
scoutpath         = /var/local/consist2.txt

[ntp]
ntp             = 1

[localconfig]
keyboard         = us

#*************************************************
# END OF MANDATORY SETTINGS FOR CCU TO HA COMMS
#*************************************************

################################################################
#Complete all DNS / Domain details here
################################################################

[site]
nameserver       = 127.0.0.1

[cmsportal]
landingpage_host = www.viarailwifi.ca
#landingpage_tail = ?id=language


[domain]
domain_name      = $landingpage_host
nameserver_ip    = $ip_portal
##clay-v2 doesn't have server_name or domain_ip
server_name      = $ip_portal
domain_ip        = $ip_portal

[dns]
#NO DNS SERVERS NEED TO BE SPECIFIED, HEADER HAS TO REMAIN

[dnsmasq]
dns-redirect        = ($ip_portal +1)
mgt-bypass        = y

[named]
listen-port = 1053

##Host file addition

[hosts]
www.viarailwifi.ca = $ip_portal
viarailwifi.ca = $ip_portal

################################################################
#Complete all Network details here
################################################################

# [networks]
# # eth0 is the F19P backplane conenction to the portal
# # eth1 and eth2 are the F19P ports for the Motorola APs
# # eth5 and eth6 are the ports on the F211. 

# br0       = eth1 eth2 eth5 eth6 192.168.98.1 netmask 255.255.255.0 mtu 1500
# br0.100   = ($standby_1) netmask 255.255.252.0 mtu 1350
# br0.101   = ($standby_2) netmask 255.255.255.0 mtu 1350
# br0.102   = ($standby_3) netmask 255.255.255.0 mtu 1350
# ##MM - Note SuperWAN (inter-CCU-Comms) must be larger MTU size than NAGTun
# br0.103   = ($ip_inter_ccu_comms) netmask 255.255.254.0 mtu 1500
# br0.104   = ($standby_4) netmask 255.255.255.255 mtu 1350
# ## br0.199 added as a dummy VLAN to seperate global_ip from Mgmt
# br0.199   = ($global_ip) netmask 255.255.255.0 mtu 1350

# ## Backplane interface to the portal blade
# eth0      = ($ip_portal -1) netmask 255.255.255.0 mtu 1350

[networks]
#Eth1 and Eth2 are the ports for the Moto AP's
#Eth3 is the WCB200
#br0 =           eth1 eth2 eth5 192.168.98.1 netmask 255.255.255.0 mtu 1500
br0 =           eth1 eth5 192.168.98.1 netmask 255.255.255.0 mtu 1500
br0.100 =       ($standby_1) netmask 255.255.252.0 mtu 1350
br0.101 =       ($standby_2) netmask 255.255.255.0 mtu 1350
br0.102 =       ($standby_3) netmask 255.255.255.0 mtu 1350
##MM - Note SuperWAN (inter-CCU-Comms) must be larger MTU size than NAGTun
br0.103 =       ($ip_inter_ccu_comms) netmask 255.255.254.0 mtu 1500
br0.104 =       ($standby_4) netmask 255.255.255.255 mtu 1350
## br0.199 added as a dummy VLAN to seperate global_ip from Mgmt
br0.199 =       ($global_ip) netmask 255.255.255.0 mtu 1350

## CMS Server interface (to 2nd F19 card)
eth0 =          ($ip_portal -1) netmask 255.255.255.0 mtu 1350

##WCB Configuration for WCB Default IP address (needs changing for production)
eth2 =          192.168.99.2 netmask 255.255.255.0 mtu 1350
eth2.10 =       10.10.10.2 netmask 255.255.255.0 mtu 1500
eth2.20 =       10.10.20.2 netmask 255.255.255.0 mtu 1500





## FOR BENCHES ONLY:
## default device (Ethernet WAN)
#eth6      = 1.1.1.1 netmask 255.255.255.0 


##################################################
#Complete all ClientNet details here
##################################################

##Nomad Management subnet
[clientnet1]
if =            br0.100
lo_offset =     2
lease =         4200
failover =      auto1
virtual_mac =   02:c0:de:01:01:01
virtual_ip =    $ip_mgmt netmask 255.255.252.0

##VIA Applications-1 Subnet (for future use)
[clientnet2]
if =            br0.101
lo_offset =     2
lease =         4200
failover =      auto1
virtual_mac =   02:c0:de:02:02:02
virtual_ip =    $ip_apps1 netmask 255.255.255.0

##VIA First Class (limited to 128 devices)
[clientnet3]
if =            br0.102
lo_offset =     2
aaa =           click_through
redirect_url =  http://($landingpage_host)/?id=language
lease =         4200
failover =      auto2
virtual_mac =   02:c0:de:03:03:03
virtual_ip =    $ip_apps2 netmask 255.255.255.128

##################################################
#Complete Metering & Throttling details here
##################################################
default_persistent_session_duration = 300
default_persistent_session_hard_data_usage_cap = 0
default_persistent_session_soft_data_usage_cap = 75
default_persistent_session_throttle_data_rate = 512
default_persistent_session_throttle_duration = 300
default_maximum_data_rate = 2048
bwlimit = m

##VIA Standard class
[clientnet4]
if =            br0.104
lo_offset =     2
aaa =           click_through
redirect_url =  http://($landingpage_host)/?id=language
lease =         4200
failover =      auto1
virtual_mac =   02:c0:de:04:04:04
virtual_ip =    $ip_clients netmask 255.255.254.0

##################################################
#Complete Metering & Throttling details here
##################################################
default_persistent_session_duration = 300
default_persistent_session_hard_data_usage_cap = 0
default_persistent_session_soft_data_usage_cap = 75
default_persistent_session_throttle_data_rate = 512
default_persistent_session_throttle_duration = 300
default_maximum_data_rate = 1024
bwlimit = m

################################################################
#Complete all Landing Page details here
################################################################

#Note: uigateway is required or the user gets no redirect
[uigateway]
remote_aaa           = 127.0.0.1
remote_aaa_port      = 8000
timeout              = 50000

[webserver]
webserver            = on
webroot              = /var/www/htdocs

[webcache]
webcache             = off
http_port            = 3128 transparent
cache_mem            = 50MB


[aaad]
enable_user_accounts = no
remote_access_controller_port = 8001
remote_access_controller_timeout = 45000
noc_soap_url = http://aaa.justabitoftyping.com/ccu6.svc
#noc_soap_url = http://192.168.153.33/ccu7.svc
noc_soap_timeouts = 2000, 2000, 5000, 10000
compress_noc_soap_responses = yes
uigateway_query_port = 8000
operating_company = Eurostar
operating_company_security_token = gFKCYzuyvbBpoz7M
operating_company_realm_prefix = ES
media_server_ip_address_lookup = /var/local/media_controller/vlan%d
debug = /dev/tty
enable_debug_commands = yes
ignore_wan_failures = yes

[accesscontroller]
log_aaa_actions =               yes
aaad_query_port =               8001
default_idle_timeout =          4200
client_link_scan_interval =     15000
client_link_scan_timeout =      5000
client_link_timeout =           1200000
accounting_queue_threads =      1
compact_accounting_queue =      yes
telemetry_conf_section =        rtclientd.aaa.ack
authenticate_free_sessions_remotely = no

[walledgarden]
whitelist           = tcp, ($landingpage_host), 80
+whitelist          = tcp, ($landingpage_host), 443
#+whitelist 			= tcp, www.apple.com, 80

#Richardtests------
#whitelist           = tcp, www.blue.com, 80
#+whitelist           = tcp, www.purple.com, 80
#Richardtests------


[webredirect]
web_server_port =                       8080
log_trapped_url =                       yes
enable_apple_captive_network_support = no



[landing_page]
landing_page             = off

[lighttpd]
#for remote portal
fastcgi.server =<< \( ".fcgi" => \( "localhost" => \(
 "bin-path" => "/var/www/htdocs/cgi-bin/uigateway.fcgi",
 "min-procs" => 4,
 "max-procs" => 16,
 "socket" => "/tmp/light_fcgi.socket",
 "check-local" => "disable",
 "kill-signal" => 9,
 "idle-timeout" => 30,
\)\)\)
>>

fastcgi.server =  <<\( "/fastcgi/monitor.fcgi" => \(\(
 "bin-path" => "/usr/local/bin/monitor_cgi",
 "max-procs" => 1,
 "socket" => "/tmp/monitor_fcgi.socket",
 "check-local" => "disable"
\)\)\)

fastcgi.server +=\( ".php" => \( "localhost" => \(
 "bin-path" => "/usr/bin/php-cgi",
 "socket" => "/tmp/php.socket",
\)\)\)

# for remote portal
fastcgi.server +=\( ".fcgi" => \( "localhost" => \(
 "bin-path" => "/usr/local/wwwbin/uigateway.fcgi",
 "min-procs" => 4,
 "max-procs" => 16,
 "socket" => "/tmp/light_fcgi.socket",
 "check-local" => "disable",
 "kill-signal" => 9,
 "idle-timeout" => 30,
\)\)\)

fastcgi.server += \( ".sh" => \( "localhost" => \(
 "bin-path" => "/usr/local/wwwbin/bash.fcgi",
 "min-procs" => 4,
 "max-procs" => 16,
 "socket" => "/tmp/light_sh.socket",
 "check-local" => "disable",
 "kill-signal" => 9,
 "idle-timeout" => 30
\)\)\)
>>

################################################################
#Complete all Driver / APN details here
################################################################


[rogers.lte]
apn =         m2minternet.apn

[bell.lte]
apn =         mnet.bell.ca.ioe

[mc8801]
tier_0 =         0
tier_1 =         200000
tier_2 =         800000
response_timeout =     5000

[mc7710]
tier_0 =                0
tier_1 =                200000
tier_2 =                800000
response_timeout =      5000

################################################################
#Complete any miscellaneous details here
################################################################

[port-forward]
#Use 8022->22 for OTEv1, and 8022->2510 for OTEv2
tcp:8022         = ($ip_portal):2510

[gps]
debug            = /dev/tty5

################################################################
#Complete files section details here
################################################################
##################################################

[files]

/etc/rc.d/rc.local,0700,root:root = <<
#!/bin/sh
touch /var/lock/subsys/local
echo 1 > /proc/sys/net/ipv4/ip_forward

##SNMP added to give CCU a unique SNMP hostname
snmpset -v 2c -c private localhost sysName.0 s VIA-CCU-($designation)

#NAT traffic coming from CCU
iptables -t nat -A POSTROUTING -o nagtun+ -j MASQUERADE

#Disallow access from client subnet to Management and other VLAN's
iptables -A INPUT -s $ip_clients/23 -d $ip_mgmt/22 -j DROP
iptables -A INPUT -s $ip_clients/23 -d $ip_apps1/24 -j DROP
iptables -A INPUT -s $ip_clients/23 -d $ip_apps2/25 -j DROP
iptables -A INPUT -s $ip_apps2/25  -d $ip_mgmt/22 -j DROP
iptables -A INPUT -s $ip_apps2/25  -d $ip_apps1/24 -j DROP
#iptables -A INPUT -s $ip_apps2/25  -d $ip_apps2/25 -j DROP
iptables -A INPUT -s $ip_apps2/25  -d $ip_clients/23 -j DROP

iptables -A FORWARD -s $ip_clients/23 -d $ip_mgmt/22 -j DROP
iptables -A FORWARD -s $ip_clients/23 -d $ip_apps1/24 -j DROP
iptables -A FORWARD -s $ip_clients/23 -d $ip_apps2/25 -j DROP
iptables -A FORWARD -s $ip_apps2/25  -d $ip_mgmt/22 -j DROP
iptables -A FORWARD -s $ip_apps2/25  -d $ip_apps1/25 -j DROP

#iptables -A FORWARD -s $ip_apps2/25  -d $ip_apps2/25 -j DROP
iptables -A FORWARD -s $ip_apps2/25  -d $ip_clients/23 -j DROP

#Below rules applied to block access from passenger subnet
iptables -I INPUT -s $ip_clients/23  -p tcp -m multiport --dport 20,21,22,23,161,162,1053,111,26,49153,8005 -d $ip_clients -j DROP
iptables -I INPUT -s $ip_clients/23  -p udp -m multiport --dport 161,162 -d $ip_clients -j DROP
iptables -I INPUT -s $ip_clients/23  -p tcp -m multiport --dport 20,21,22,23,1053,111,26,49153,8005 -d $global_ip -j DROP
iptables -I INPUT -s $ip_apps2/25   -p tcp -m multiport --dport 20,21,22,23,1053,111,26,49153,8005 -d $global_ip -j DROP
iptables -I INPUT -s $ip_clients/23  -p tcp -m multiport --dport 20,21,22,23,1053,111,26,49153,8005 -d $ip_portal -j DROP
iptables -I INPUT -s $ip_apps2/25   -p tcp -m multiport --dport 20,21,22,23,1053,111,26,49153,8005 -d $ip_portal -j DROP

##Added to stop ping-Flood attacks
iptables -I FORWARD -p icmp --icmp-type echo-request -m limit --limit 3/s -j ACCEPT

##Added to stop Zabbix from potentially scanning WAN's
iptables -A OUTPUT -p tcp -o nagtun+ --dport 161 -j DROP
iptables -A OUTPUT -p udp -o nagtun+ --dport 161 -j DROP

chmod -R 1777 /tmp

iptables -t filter -I FORWARD -d 8.8.8.8 -j ACCEPT
iptables -t filter -I FORWARD -d ($ip_portal) -j ACCEPT

# Don't cache portal server
iptables -t nat -I PREROUTING -d ($ip_portal) -j ACCEPT
iptables -t nat -I PREROUTING -d ($ip_mgmt) -j ACCEPT
iptables -t nat -I PREROUTING -d ($ip_apps1) -j ACCEPT
iptables -t nat -I PREROUTING -d ($ip_apps2) -j ACCEPT
iptables -t nat -I PREROUTING -d ($ip_clients) -j ACCEPT

sleep 2

##This line added to fix a bug with Excess Usage Policy when used with OTE
iptables -t mangle -I POSTROUTING -d ($ip_clients -1)/23 -s ($ip_portal -2)/24 -j ACCEPT
iptables -t mangle -I POSTROUTING -d ($ip_apps2 -1)/25 -s ($ip_portal -2)/24 -j ACCEPT

# Locally access portal blade by ssh
iptables -t nat -A PREROUTING -i eth2 -p tcp -m tcp --dport 8022 -j DNAT --to-destination $ip_portal:2510

##Following lines added for start Zabbix
/conf/scripts/zabbix_proxy.sh &

/etc/rc.d/init.d/dnsmasq restart

# Run NTP fix to start NTP service only after WAN's are up
/conf/scripts/ntp_fixes.sh

#Run DNS diags
/conf/dnsmasq_monitor 2>&1 >/dev/null &


>>

