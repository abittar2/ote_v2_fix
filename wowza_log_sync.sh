#!/bin/bash

WOWZA_SERVER_IP=`cat /etc/portal/wowza_ip`

MyDesignation=$(/bin/cat /conf/ME.conf | /bin/grep "CCU_name " | /usr/bin/cut -d "=" -f 2 | /bin/sed 's/ //g')
/usr/bin/rsync --timeout=60 -bazvP /mnt/md0/ND/WowzaMediaServer/logs/wowzamediaserver_access* ${WOWZA_SERVER_IP}::wowza-logs/$MyDesignation

logger 'Wowza stats files syncd'
